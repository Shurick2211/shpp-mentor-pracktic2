package com.onimko;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {
  private static final Logger log = LoggerFactory.getLogger(Main.class);
  public static void main(String ... args) {
      log.info("App was start!");
      try {
          Table table = Table.getTableInstance();
          table.displayTable();
      } catch (NullPointerException e) {
          log.error("Unknown type of number in input's var!");
          throw new MyRuntimeException();
      }
      log.info("App finished with success!");
  }
}