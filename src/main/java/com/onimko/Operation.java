package com.onimko;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * The POJO class for an operation.
 */
public class Operation {
    /**First number for the operation*/
    private final Number x;
    /**Second number for the operation*/
    private final Number y;
    /**The result for the operation*/
    private Number result;

    /**
     *  The constructor for creates the operation.
     */
    public Operation(Number x, Number y) {
        this.x = x;
        this.y = y;
    }

    /**
     * The multiply operation.
     */
    public Operation multiply() {
        if (x.getClass().equals(Float.class) || x.getClass().equals(Double.class))
            result = x.toString().length() > 9 || y.toString().length() > 9 ?
                    new BigDecimal(x.toString()).multiply(new BigDecimal(y.toString())):
                    x.doubleValue() * y.doubleValue();
        else
            result = x.toString().length() > 10 || y.toString().length() > 10 ?
                    new BigInteger(x.toString()).multiply(new BigInteger(y.toString())):
                    x.longValue() * y.longValue();
        return this;
    }

    /**
     * Method for returns the result of the operation.
     * @return The result number.
     */
    public Number getResult() {
        return result;
    }

    /**
     * The override method object's class.
     * @return The operations as string.
     */
    @Override
    public String toString() {
        return x + " * " + y + " = " + result;
    }
}
