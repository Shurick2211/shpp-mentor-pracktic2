package com.onimko;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The class for table.
 */
public class Table {
    /**The logger for this class*/
    private static final Logger log = LoggerFactory.getLogger(Table.class);
    /**The file of properties for default*/
    private static final String FILE_PROP = "my.properties";
    /**The result's table*/
    private final Operation[][] resultTable;

    private static final String INCREMENT = "increment";
    private static final String MIN = "min";
    private static final String MAX = "max";

    /**
     * The constructor for creates the table.
     * @param min - the minimal value in the table.
     * @param max - the maximal value in the table.
     * @param increment - the step of value in the table.
     * @param isInt - value of the table is byte or short or integer or long.
     */
    private Table(Number min, Number max, Number increment, boolean isInt) {
        int i = (int)((max.doubleValue() - min.doubleValue()) / increment.doubleValue()) + 1;
        resultTable = new Operation[i][i];
        for (int x = 0; x < resultTable.length; x++)
            for (int y = 0; y < resultTable.length; y++){
                resultTable[x][y] = isInt ?
                        new Operation(min.longValue() + x * increment.longValue(),
                                min.longValue() + y * increment.longValue()).multiply() :
                        new Operation(min.doubleValue() + x * increment.doubleValue(),
                                min.doubleValue() + y * increment.doubleValue()).multiply();
            }
    }

    /**
     * Method for returns the result's table.
     * @return the table.
     */
    public Operation[][] getResultTable() {
        return resultTable;
    }

    /**
     * Method prints table in the long.
     */
    public void displayTable() {
        for (int x = 0; x < resultTable.length; x++)
            for (int y = 0; y < resultTable.length; y++){
                log.info("{}", resultTable[x][y]);
            }
    }

    /**
     * The fabric method for create Table-instance.
     * @param fileProperties The name of properties-file.
     * @return Table-instance.
     */
    public static Table getTableInstance(String fileProperties)  {
        LoadProperties prop = new LoadProperties(fileProperties);
        try {
            switch (prop.getTypeProperty()) {
                case "byte": {
                    Table table = new Table(Byte.parseByte(prop.getProperty(MIN)),
                            Byte.parseByte(prop.getProperty(MAX)),
                            Byte.parseByte(prop.getProperty(INCREMENT)),true);
                    log.debug("Byte table!");
                    return table;
                }
                case "short": {
                    Table table = new Table(Short.parseShort(prop.getProperty(MIN)),
                            Short.parseShort(prop.getProperty(MAX)),
                            Short.parseShort(prop.getProperty(INCREMENT)),true);
                    log.debug("Short table!");
                    return table;
                }
                case "int": {
                    Table table = new Table(Integer.parseInt(prop.getProperty(MIN)),
                            Integer.parseInt(prop.getProperty(MAX)),
                            Integer.parseInt(prop.getProperty(INCREMENT)),true);
                    log.debug("Int table!");
                    return table;
                }
                case "long": {
                    Table table = new Table(Long.parseLong(prop.getProperty(MIN)),
                            Long.parseLong(prop.getProperty(MAX)),
                            Long.parseLong(prop.getProperty(INCREMENT)),true);
                    log.debug("Long table!");
                    return table;
                }
                case "float": {
                    Table table = new Table (Float.parseFloat(prop.getProperty(MIN)),
                            Float.parseFloat(prop.getProperty(MAX)),
                            Float.parseFloat(prop.getProperty(INCREMENT)),false);
                    log.debug("Float table!");
                    return table;
                }
                case "double": {
                    Table table = new Table(Double.parseDouble(prop.getProperty(MIN)),
                            Double.parseDouble(prop.getProperty(MAX)),
                            Double.parseDouble(prop.getProperty(INCREMENT)),false);
                    log.debug("Double table!");
                    return table;
                }
                default: {
                    return null;
                }
            }
        } catch (NumberFormatException e) {
            log.error("Wrong type input's properties!",e);
            throw new MyRuntimeException();
        }
    }

    /**
     * The overload fabric method for create Table-instance
     * with file of properties for default.
     * @return Table-instance.
     */
    public static Table  getTableInstance() throws MyRuntimeException{
        return getTableInstance(FILE_PROP);
    }
}