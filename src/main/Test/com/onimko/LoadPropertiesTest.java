package com.onimko;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;

public class LoadPropertiesTest {

    @Test
    public void getProperty() {
        LoadProperties properties = new LoadProperties("testDouble.properties");
        assertEquals("2.2", properties.getProperty("min"));
        assertEquals("0.2", properties.getProperty("increment"));
        assertThrows(NullPointerException.class, () -> new LoadProperties("ddd.ff"));
    }

    @Test
    public void getTypeProperty() {
        LoadProperties properties = new LoadProperties("my.properties");
        assertEquals("int", properties.getTypeProperty());

        System.setProperty("type","long");
        assertEquals("long", properties.getTypeProperty());

        System.setProperty("type","ddd");
        assertEquals("ddd", properties.getTypeProperty());
    }


}