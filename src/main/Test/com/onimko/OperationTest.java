package com.onimko;

import org.junit.Test;

import java.math.BigDecimal;
import java.math.BigInteger;

import static org.junit.Assert.*;

public class OperationTest {

    @Test
    public void getResult() {
        assertEquals(10l,new Operation(5,2).multiply().getResult());
        assertTrue(10d - new Operation(4.0,2.5).multiply().getResult().doubleValue() < 0.000000000000001);
        assertEquals(4611686014132420609l,
                new Operation(Integer.MAX_VALUE,Integer.MAX_VALUE).multiply().getResult());
        assertEquals(new BigInteger("111111111111111111"),
                new Operation(37037037037037037l,3).multiply().getResult());
        assertEquals(1, new BigDecimal("0.11111111111111111111")
                        .compareTo((BigDecimal) new Operation(0.37037037037037037037,0.3)
                                .multiply().getResult()));
    }

    @Test
    public void testToString() {
        assertEquals("10 * 10 = 100", new Operation(10,10).multiply().toString());
    }
}