package com.onimko;

import org.junit.Test;

import static org.junit.Assert.*;

public class TableTest {

    @Test
    public void getTable() {
        System.setProperty("type","byte");
        Table table1 = Table.getTableInstance();
        assertEquals(5,table1.getResultTable().length);
        assertEquals(5,table1.getResultTable()[0].length);
        System.setProperty("type","double");
        Table table2 = Table.getTableInstance("testDouble.properties");
        assertEquals(6,table2.getResultTable().length);
        assertEquals(6,table2.getResultTable()[0].length);
    }

    @Test
    public void getTableInstance() {
        System.setProperty("type","byte");
        Table table1 = Table.getTableInstance();
        assertTrue(table1.getClass().isInstance(Table.getTableInstance()));

        System.setProperty("type","short");
        Table table2 = Table.getTableInstance();
        assertTrue(table2.getClass().isInstance(Table.getTableInstance()));

        System.setProperty("type","int");
        Table table3 = Table.getTableInstance();
        assertTrue(table3.getClass().isInstance(Table.getTableInstance()));

        System.setProperty("type","long");
        Table table4 = Table.getTableInstance();
        assertTrue(table4.getClass().isInstance(Table.getTableInstance()));

        System.setProperty("type","float");
        Table table5 = Table.getTableInstance();
        assertTrue(table5.getClass().isInstance(Table.getTableInstance()));

        System.setProperty("type","double");
        Table table6 = Table.getTableInstance();
        assertTrue(table6.getClass().isInstance(Table.getTableInstance()));

        System.setProperty("type","ddd");
        Table table7 = Table.getTableInstance();
        assertThrows(NullPointerException.class,() -> table7.getClass());

        System.setProperty("type","int");
        assertThrows(MyRuntimeException.class,
                () -> Table.getTableInstance("testDouble.properties"));
    }
}